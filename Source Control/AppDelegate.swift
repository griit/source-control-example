//
//  AppDelegate.swift
//  Source Control
//
//  Created by Moog Kwon on 2/13/19.
//  Copyright © 2019 Moog Kwon. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

